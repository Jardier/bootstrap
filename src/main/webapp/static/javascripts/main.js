var BootStrap = BootStrap || {};

BootStrap.Main = (function() {

	function Main() {
		this.tooltip = $('[data-toggle="tooltip"]');
	}

	Main.prototype.iniciar = function() {
		this.tooltip.tooltip();
	}

	return Main;

})();

BootStrap.DatePicker = (function() {

	function DatePicker() {
		this.componenteDate = $('.js-datepicker');
	}

	DatePicker.prototype.enable = function() {
		this.componenteDate.datepicker({
			format : 'dd/mm/yyyy',
			autoclose : true,
		});
	}

	return DatePicker;

})();

$(function() {
	var main = new BootStrap.Main();
	main.iniciar();

	var datePicker = new BootStrap.DatePicker();
	datePicker.enable();

});
